import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import TodoItem from './TodoItem'
import TodoAdd from './TodoAdd'
import Paper from '@material-ui/core/Paper'
import List from '@material-ui/core/List'

const Todos = () => {
  const apiUrl = 'http://jsonplaceholder.typicode.com/todos'
  const [todos, setTodos] = useState([])

  const getTodos = () => {
    axios.get(`${apiUrl}?_limit=20`)
      .then(response => {
        setTodos(response.data)
      })
  }

  const toggleComplete = (id) => {
    setTodos(todos.map(todo => {
      if (todo.id === id) {
        todo.completed = !todo.completed
      }

      return todo
    }))
  }

  const deleteTodoItem = (id) => {
    axios.delete(`${apiUrl}/${id}`)
      .then(response => {
        setTodos(todos.filter(todo => todo.id !== id))
      })
  }

  const addTodoItem = (title) => {
    axios.post(apiUrl, {
      title,
      completed: false
    }).then(response => {
      setTodos([...todos, response.data])
    })
  }

  useEffect(() => {
    getTodos()
  }, [])

  return (
    <>
      <Paper>
        <List>
          {todos.map((todo) => (
            <TodoItem
              key={todo.id}
              id={todo.id}
              title={todo.title}
              completed={todo.completed}
              toggleComplete={toggleComplete}
              deleteTodoItem={deleteTodoItem}
            />
          ))}
        </List>
      </Paper>

      <TodoAdd addTodoItem={addTodoItem} />
    </>
  )
}

Todos.propTypes = {
  todos: PropTypes.array,
  getTodos: PropTypes.func,
  toggleComplete: PropTypes.func,
  deleteTodoItem: PropTypes.func,
  addTodoItem: PropTypes.func
}

export default Todos
