import React, {useState} from 'react'
import PropTypes from 'prop-types'
import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment'
import IconButton from '@material-ui/core/IconButton'
import Add from '@material-ui/icons/Add'

const TodoAdd = ({addTodoItem}) => {
    const [title, setTitle] = useState('')

    const onChangeTitle = (event) => {
        setTitle(event.target.value)
    }

    const submit = (event) => {
        event.preventDefault()
        addTodoItem(title)
        setTitle('')
    }

    return (
        <form style={{ marginTop: '20px' }}>
            <TextField
                id="add-todo"
                variant="outlined"
                type="text"
                value={title}
                onChange={onChangeTitle}
                label="Add todo"
                fullWidth
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <IconButton onClick={submit}>
                                <Add />
                            </IconButton>
                        </InputAdornment>
                    )
                }}
            />
        </form>
    )
}

TodoAdd.propTypes = {
    title: PropTypes.string,
    onChangeTitle: PropTypes.func,
    submit: PropTypes.func
}

export default TodoAdd
