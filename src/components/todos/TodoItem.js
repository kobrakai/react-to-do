import React from 'react'
import PropTypes from 'prop-types'
import Checkbox from '@material-ui/core/Checkbox'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'


const TodoItem = ({
    id,
    title,
    completed,
    toggleComplete,
    deleteTodoItem
}) => {
    const getStyle = () => {
        return {
            textDecoration: completed ? 'line-through' : ''
        }
    }

    return (
        <ListItem key={id} dense button onClick={toggleComplete.bind(this, id)}>
            <ListItemIcon>
                <Checkbox checked={completed} />
            </ListItemIcon>

            <ListItemText id={id} primary={title} style={getStyle()} />

            <ListItemSecondaryAction>
                <IconButton aria-label="delete" onClick={deleteTodoItem.bind(this, id)}>
                    <DeleteIcon />
                </IconButton>
            </ListItemSecondaryAction>
            
        </ListItem>
    )
}

TodoItem.propTypes = {
    getStyle: PropTypes.object
}

export default TodoItem
