import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Container from '@material-ui/core/Container'

const Header = () => {
    return (
        <AppBar position="static" style={{marginBottom: '30px'}}>
            <Container>
                <Toolbar>
                    <IconButton edge="start" color="inherit" aria-label="menu">
                        <MenuIcon />
                    </IconButton>

                    <Typography variant="h6">
                        Todo
                    </Typography>
                </Toolbar>
            </Container>
        </AppBar>
    )
}

export default Header