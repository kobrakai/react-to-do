import React from 'react'
import Header from './components/layout/Header'
import Todos from './components/todos/Todos'
import Container from '@material-ui/core/Container'
import './App.css'

const App = () => {
  return (
    <>
      <Header />

      <Container>
        <Todos />
      </Container>
    </>
  )
}

export default App
